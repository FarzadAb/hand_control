#!/bin/bash

# Setting up opensim python env and the models
### Please make sure to run this code in the root directory of the (cloned) repistory so that the models are setup correctly **

# Installing Miniconda for Python3 (64 bit Linux version)
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh  # need to accept license and installation prefix
rm Miniconda3-latest-Linux-x86_64.sh

yes | conda create -n opensim-rl -c kidzik opensim git
source activate opensim-rl

yes | conda install -c conda-forge lapack git
pip install git+https://github.com/stanfordnmbl/osim-rl.git

python -c "import opensim"  # test correct setup

mkdir -p models

# Downloading some OpenSim models
git clone https://github.com/opensim-org/opensim-models -o models/opensim

# place the Geometry files beside the actual model (it can't seem to find it otherwise and will only display the muscles without the bones)
for m in models/opensim/Models/*;
do
    pushd $m
    ln -s ../../Geometry .
    popd
done