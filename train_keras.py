# source activate opensim-rl
import numpy as np
import gym

from arm import ArmEnv
from agent import MyDDPGAgent

# ENV_NAME = 'Pendulum-v0'
# gym.undo_logger_setup()


# Get the environment and extract the number of actions.
# env = gym.make(ENV_NAME)
ENV_NAME = 'ArmEnv-v0'
env = ArmEnv()
np.random.seed(123)
env.seed(123)

agent = MyDDPGAgent(env)

last_saved = 'TH_1_1'
try:
    agent.load(last_saved)
except:
    print('####\n\n\tCould not load saved data file\n\n####')

for i in range(100):
#    try:
    agent.fit(env, 40000, verbose=2, nb_max_episode_steps=500)
#    except:
#        print('Caught error, re-running from:' + last_saved)
#        agent.load(last_saved)
#        env.reset()
#        continue
    # After training is done, we save the final weights.
    #agent.save(ENV_NAME)
    last_saved = 'TH_3_' + str(i)
    agent.save(last_saved)

# Finally, evaluate our algorithm for 5 episodes.
#agent.test(ArmEnv(True))

#########
# energy_fix1_0: worked, but just stays close, doesn't actually get to the circle, stops midway
# reward: -l_task - l_energy
# energy_fix2_0: goes into circle, tries to localy do a circle, but really minimal
# reward: -l_task - l_energy/5
# ** energy_fix_2_1 was a little weird, wouldn't go into the circle anymore
# energy_fix3_0: 
# l_energy = np.sum(np.square(np.maximum(np.subtract(action, 0.15), 0)))
# energy_fix3_9: goes over a really small circle (outside of the red cylinder)
# ** bug: NPhases was wrong (set to a constant 20)
# fix: getting num_phases from kinematics parameters
# energy_fix4_2: does a small noisy back-and-forth but not really accomplishes the task
# reward: -3 * l_task - l_energy/7
# ** the next step would be to add velocity reward (TODO: figure out dt)
# ** or changing the target cylinder position and radius
# energy_fix5_1: still outside the circle but goes up and down a lot (even outside the circle: maybe the circle is not correct?)
# reward: -3 * l_task - l_energy / 7 - l_vel/5
# l_vel: np.sum(np.square(np.subtract(self.get_kin_diff(), endpoint_diff) / self.stepsize))
# Adam lr: 0.0003 -> 0.0001
# energy_fix6_0: not much different
# reward: -4 * l_task - l_energy / 7 - l_vel / 2
# l_energy = np.sum(np.square(np.maximum(np.subtract(action, 0.25), 0)))


######## starting a new learning thread
# T2_1_2: took a little bit to learn at first, but I like this better: it actually does track something (not passive), but really bad
# target: CircleParams(0.20, -0.40, 0.20)
# Adam clipnorm: 1 -> 0.5
# %% simulator freq: 29.47 hz
# T2_2_5: it just goes up and down too much. decreasing vel (and energy) cost
# ** after running visual simulation, it seems like impossible/hard to perform motion, so changing the position as follows:
# target: CircleParams(0.40, -0.35, 0.20)
# ** counter-clockwise turn:
# get_angle: math.pi - i * self.speed
# Adam lr: 0.0001 -> 0.00008 (since I got NaN)
# T3_ : 
# reward: -4 * l_task - l_energy / 10 - l_vel / 10
