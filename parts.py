'''
    Wrapper objects for OpenSim objects
'''

class Joint(object):
    '''
        This class is just a wrapper around the OpenSim joint object
        in order to make things simpler to work with
    '''
    NumStateVars = 2
    def __init__(self, joint):
        self._joint = joint
        # TODO: set this if we can
        # self.observation_space
    def get_state(self, sim_state):
        # TODO: check if we need sanitification
        pos = self._joint.getCoordinate(0).getValue(sim_state)
        vel = self._joint.getCoordinate(0).getSpeedValue(sim_state)
        # acc = self._joint.getCoordinate(0).getAccelerationValue(sim_state)
    
        return pos, vel#, acc


class Body(object):
    '''
        This class is just a wrapper around the OpenSim body object
        in order to make things simpler to work with
    '''
    def __init__(self, body):
        self._body = body
        # TODO: set this if we can
        # self.observation_space
    def get_state(self, sim_state):
        # TODO: check if we need sanitification
        pos = vec_to_array(self._body.getPositionInGround(sim_state))
        gv = self._body.getVelocityInGround(sim_state)
        w = vec_to_array(gv.get(0))  # omega
        vel = vec_to_array(gv.get(1))
        # acc = vec_to_array(self._body.getAccelerationInGround(sim_state))
    
        return pos, w, vel#, acc


def vec_to_array(vec):
    '''
        Transforms SimTK::Vec objects (from C++) into friendly python arrays
    '''
    return [vec[i] for i in range(vec.size())]