import matplotlib.pyplot as plt
plt.ion()


class DynamicPlotter(object):

    def __init__(self, xlim, ylim):
        #Set up plot
        self.figure, self.ax = plt.subplots()
        self.lines = [None] * 2
        self.lines[0], = self.ax.plot([], [], 'g-')
        self.lines[1], = self.ax.plot([], [], 'r-')
        self.data = [
            {"x": [], "y": []},
            {"x": [], "y": []},
        ]
        #Autoscale on unknown axis and known lims on the other
        self.ax.set_autoscaley_on(True)
        self.ax.set_xlim(xlim[0], xlim[1])
        self.ax.set_ylim(ylim[0], ylim[1])

    def add_points(self, pos, target):
        #Update data (with the new _and_ the old points)
        self.data[0]["x"].append(pos[0])
        self.data[0]["y"].append(pos[1])
        self.lines[0].set_xdata(self.data[0]["x"])
        self.lines[0].set_ydata(self.data[0]["y"])
        self.data[1]["x"].append(target[0])
        self.data[1]["y"].append(target[1])
        self.lines[1].set_xdata(self.data[1]["x"])
        self.lines[1].set_ydata(self.data[1]["y"])
        #Need both of these in order to rescale
        self.ax.relim()
        self.ax.autoscale_view()
        #We need to draw *and* flush
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()
