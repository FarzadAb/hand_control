# source activate opensim-rl
import numpy as np
import gym
import sys

from arm import ArmEnv
from agent import MyDDPGAgent

# ENV_NAME = 'Pendulum-v0'
# gym.undo_logger_setup()


# Get the environment and extract the number of actions.
# env = gym.make(ENV_NAME)
# ENV_NAME = 'ArmEnv-v0'
env = ArmEnv(True)
np.random.seed(123)
env.seed(123)

agent = MyDDPGAgent(env)

agent.load(sys.argv[1])

agent.test(env, 100)

input("Press Enter to close the program...")