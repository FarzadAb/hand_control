#!/bin/bash

mkdir -p build

cd build

rootpath=`pwd`

# git clone https://github.com/simbody/simbody simbody-source
# mkdir -p simbody-build simbody

# pushd simbody-source
# # git checkout Simbody-3.6
# popd

# pushd simbody-build
# cmake ${rootpath}/simbody-source \
#     -DCMAKE_INSTALL_PREFIX=${rootpath}/simbody \
#     -DCMAKE_BUILD_TYPE=Release 
# make -j4  # takes a whole while
# make -j4 install
# popd

# # TODO: add to /usr/bin ??

# home_env_cmd="SIMBODY_HOME=${rootpath}/simbody"
# `home_env_cmd`
# echo "$home_env_cmd" > ~/.zshrc
# echo "$home_env_cmd" > ~/.bashrc

git clone https://github.com/opensim-org/opensim-core opensim-core-source
mkdir -p opensim-core-build opensim-core opensim-deps-build opensim-deps-install

pushd opensim-deps-build
cmake ${rootpath}/opensim-core-source/dependencies \
    -DCMAKE_INSTALL_PREFIX=${rootpath}/opensim-deps-install \
    -DCMAKE_BUILD_TYPE=Release
make -j4
make -j4 install
popd

pushd opensim-core-build
cmake ${rootpath}/opensim-core-source \
    -DCMAKE_INSTALL_PREFIX=${rootpath}/opensim-core \
    -DCMAKE_BUILD_TYPE=Release \
    -DBUILD_PYTHON_WRAPPING=TRUE \
    -DOPENSIM_PYTHON_VERSION=3 \
    -DOPENSIM_DEPENDENCIES_DIR=${rootpath}/opensim-deps-install \
    -Ddocopt_DIR=${rootpath}/opensim-deps-install/docopt/lib64/cmake/docopt  # this shouldn't be needed, but it was
make -j4
make -j4 install
popd