import math
import numpy as np


class KinParams(object):
    '''
        Kinematic state parameters
    '''
    def __init__(self, nparams):
        self.nparams = nparams
        self.data = [[0] * nparams]
        self.nphases = 1
    def get_keyframe(self, i):
        return self.data[i % self.nphases]
    def num_phases(self):
        return self.nphases
    def get_keyframe_diff(self, i):
        return np.subtract(self.get_keyframe(i), self.get_keyframe(i-1))
    def get_velocity_direction(self, i):
        v = self.get_keyframe_diff(i)
        return np.array(v) / np.linalg.norm(v)



class CircleParams(KinParams):
    def __init__(self, center_x, center_y, radius, speed=0.01):
        self.center_x = center_x
        self.center_y = center_y
        self.radius = radius
        self.speed = speed
        self.nparams = 2
    def _get_angle_at_frame(self, i):
        return math.pi - i * self.speed
    def num_phases(self):
        return math.floor(2*math.pi / abs(self.speed))
    def get_keyframe(self, i):
        angle = self._get_angle_at_frame(i)
        return math.cos(angle) * self.radius + self.center_x, math.sin(angle) * self.radius + self.center_y
    def get_velocity_direction(self, i):
        angle = self._get_angle_at_frame(i)
        return math.sin(angle), math.cos(angle)
