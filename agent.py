import os

from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Flatten, Input, Concatenate
from keras.optimizers import Adam

from rl.agents import DDPGAgent
from rl.memory import SequentialMemory
from rl.random import OrnsteinUhlenbeckProcess

CONFIG = {
    'output_path': 'output',
    'logs_path': 'logs',
}


class MyDDPGAgent(object):
    def __init__(self, env):
        self.init_nets(env)
        # if env is not None:
        #     self.initialize(env)
    
    def init_nets(self, env):
        assert len(env.action_space.shape) == 1
        self.nb_actions = env.action_space.shape[0]

        # Next, we build a very simple model.
        self.actor = Sequential()
        self.actor.add(Flatten(input_shape=(1,) + env.observation_space.shape))
        self.actor.add(Dense(16))
        self.actor.add(Activation('relu'))
        self.actor.add(Dense(16))
        self.actor.add(Activation('relu'))
        self.actor.add(Dense(16))
        self.actor.add(Activation('relu'))
        self.actor.add(Dense(self.nb_actions))
        self.actor.add(Activation('sigmoid'))
        print(self.actor.summary())

        action_input = Input(shape=(self.nb_actions,), name='action_input')
        observation_input = Input(shape=(1,) + env.observation_space.shape, name='observation_input')
        flattened_observation = Flatten()(observation_input)
        x = Concatenate()([action_input, flattened_observation])
        x = Dense(32)(x)
        x = Activation('relu')(x)
        x = Dense(32)(x)
        x = Activation('relu')(x)
        x = Dense(32)(x)
        x = Activation('relu')(x)
        x = Dense(1)(x)
        x = Activation('linear')(x)
        self.critic = Model(inputs=[action_input, observation_input], outputs=x)
        print(self.critic.summary())
        
        # Finally, we configure and compile our agent. You can use every built-in Keras optimizer and
        # even the metrics!
        memory = SequentialMemory(limit=100000, window_length=1)
        random_process = OrnsteinUhlenbeckProcess(size=self.nb_actions, theta=.15, mu=0., sigma=.3)
        self.agent = DDPGAgent(nb_actions=self.nb_actions, actor=self.actor, critic=self.critic, critic_action_input=action_input,
                        memory=memory, nb_steps_warmup_critic=100, nb_steps_warmup_actor=100,
                        random_process=random_process, gamma=.96, target_model_update=1e-3)
        self.agent.compile(Adam(lr=.0003, clipnorm=1.), metrics=['mae'])
    
    def fit(self, env, nb_steps=50000, verbose=2, nb_max_episode_steps=500):
        self.agent.fit(
            env, visualize=True,
            nb_steps=nb_steps, nb_max_episode_steps=nb_max_episode_steps,
            verbose=verbose, log_interval=int(nb_steps/10)
        )
    
    def _get_weights_file_path(self, name=''):
        return os.path.join(CONFIG['output_path'], 'ddpg_{}.h5f'.format(name))
    
    def save(self, name='', overwrite=True):
        self.agent.save_weights(self._get_weights_file_path(name), overwrite=overwrite)
    
    def load(self, name='', file_path=None):
        if file_path is None:
            file_path = self._get_weights_file_path(name)
        self.agent.load_weights(file_path)
    
    def test(self, env, nb_episodes=5, nb_max_episode_steps=1000):
        self.agent.test(env, nb_episodes=nb_episodes, visualize=True, nb_max_episode_steps=nb_max_episode_steps)

    def forward(self, observation):
        return self.agent.forward(observation)
