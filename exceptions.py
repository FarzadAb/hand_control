class IntegrationError(Exception):
    pass

class ActionValueError(ValueError):
    pass
