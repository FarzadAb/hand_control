import opensim
import numpy as np
from osim.env.generic import OsimEnv

from parts import Joint, Body
from exceptions import IntegrationError


class BaseEnv(OsimEnv):
    timestep_limit = 200  # FIXME not sure what this is

    def __init__(self, visualize=True):
        # self.iepisode = 0
        self.joints = []
        self.bodies = []
        super(BaseEnv, self).__init__(visualize=visualize)
        self.visualize = visualize

        self.osim_model.model.initSystem()
        self.spec.timestep_limit = self.timestep_limit
        self.reset()

        if visualize:
            self.manager.setInitialTime(-0.00001)
            self.manager.setFinalTime(0.0)
            self.manager.integrate(self.osim_model.state)

    def configure(self):
        super(BaseEnv, self).configure()
        for i in range(self.osim_model.jointSet.getSize()):
            # TODO: which is better self.joints or self.osim_model.joints
            self.joints.append(
                Joint(opensim.CustomJoint.safeDownCast( self.osim_model.jointSet.get(i) ) )
            )
        for i in range(self.osim_model.bodySet.getSize()):
            self.bodies.append( Body( self.osim_model.bodySet.get(i) ) )
        self.ninput = self._n_base_inputs = (6 + Joint.NumStateVars * len(self.joints))
    
    def get_observation(self):
        obs = np.array([0] * self._n_base_inputs, dtype='f')

        pos = self.osim_model.model.calcMassCenterPosition(self.osim_model.state)
        vel = self.osim_model.model.calcMassCenterVelocity(self.osim_model.state)
        
        obs[-6:-3] = [pos[0], pos[1], pos[2]]
        obs[-3:  ] = [vel[0], vel[1], vel[2]]

        for i, joint in enumerate(self.joints):
            obs[i*Joint.NumStateVars:(i+1)*Joint.NumStateVars] = joint.get_state(self.osim_model.state)
        
        return obs
    
    def reset(self):
        self.manager = opensim.Manager(self.osim_model.model)
        return self._reset()
    
    def _step(self, action):
        self.activate_muscles(action)

        # Integrate one step
        self.manager.setInitialTime(self.stepsize * self.istep)
        self.manager.setFinalTime(self.stepsize * (self.istep + 1))

        try:
            self.manager.integrate(self.osim_model.state)
        except Exception as e:
            print (e)
            raise IntegrationError(*e.args)

        self.istep = self.istep + 1

