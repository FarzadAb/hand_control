# Make sure the env is set: source activate opensim-rl
import opensim
import numpy as np

from models import MODELS
from base import BaseEnv
from arm import ArmEnv

h = ArmEnv()
action = h.action_space.sample()
# action = np.zeros(action.shape)
for i in range(100):
    obs, reward, _, _ = h.step(action)
    print(reward)
    print(obs[-4], obs[-3], obs[-2])

input("Press Enter to close the program...")

h.reset()

for i in range(10):
    obs, reward, _, _ = h.step(action)
    print(reward)
    print(obs[-4], obs[-3], obs[-2])

input("Press Enter to close the program...")