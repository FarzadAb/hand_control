import opensim
import math
import numpy as np
import os
import random
from osim.env.generic import OsimEnv, Osim

from models import MODELS
from base import BaseEnv
from exceptions import IntegrationError, ActionValueError
from kinematics import KinParams, CircleParams
from parts import Joint
from plotter import DynamicPlotter


class ArmEnv(BaseEnv):
    model_path = MODELS['arm_circle_target']['model_path']
    #NPhases = 20
    CrashPenalty = -40 # -100
    def __init__(self, visualize = False):
        self.kparams = CircleParams(0.40, -0.35, 0.10, speed=0.02)
#        self.stepsize = 0.003
        super(ArmEnv, self).__init__(visualize = visualize)
        self.plotter = None
        self.reset()
    
    def initiate_phase(self):
        self.phase = random.randint(0, self.kparams.num_phases()-1)
    
    def configure(self):
        super(ArmEnv, self).configure()

        self.model_endpoint = self.bodies[-2]

        # removing the extra virtual joints: target and endpoint
        self.joints.pop()
        self.joints.pop()
        self.ninput -= 2*Joint.NumStateVars
        self._n_base_inputs -= 2*Joint.NumStateVars

        self.ninput += self.kparams.nparams  # number of input kinematics states
        self.ninput += 2  # number of endopoint-pos parameters
    
    def increment_phase(self):
        self.phase += 1
        #if self.phase == self.NPhases:
        #    self.phase = 0
    
    def get_kin_state(self):
        '''returns the current desired kinematics state'''
        # TODO
        return self.kparams.get_keyframe(self.phase)
    
    def get_kin_diff(self):
        return self.kparams.get_keyframe_diff(self.phase)
    
    def _get_observation(self):
        base = super(ArmEnv, self).get_observation()
        endpoint_p, _, _ = self.model_endpoint.get_state(self.osim_model.state)
        self.endpoint_pos = endpoint_p[:2]
        
        obs = np.concatenate([
            base,
            self.endpoint_pos,
            self.get_kin_state(),
        ])
        
        return obs
    
    def get_prev_observation(self):
        '''
            Returns observation in the previous step
                returns `None` in case the previous observation doesn't exist, i.e. this is the first step
        '''
        return self.observation_history.get(self.istep-1, None)

    def get_observation(self):
        obs = self.get_unsanitized_observation()
        if not np.any(np.isnan(obs)):
            return obs
        else:
            if self.get_prev_observation() is not None:
                return self.get_prev_observation()
            else:
                obs[np.isnan(obs)] = 0
                return obs

    def get_unsanitized_observation(self):
        # TODO: maybe move this to BaseEnv
        # TODO: calculate end-effector position
        if self.istep in self.observation_history:
            return self.observation_history[self.istep]

        # compute actual observation
        self.observation_history[self.istep] = self._get_observation()

        # remove old observation
        self.observation_history.pop(self.istep-2, None)
        
        return self.observation_history[self.istep]
    
    def compute_reward(self, action):
        # TODO: add weights
        # TODO: fix usage of get_kin_state
        # TODO: penalize bad poses: almost done
        # TODO: penalize co-activation of antagonistic muscles?

        # TODO: add reward for velocity in the direction of target
        # TODO: and/or add reward for velocity in the direction that target is moving
        if self.is_done():
            return self.CrashPenalty
        
        l_vel = 0
        p_obs = self.get_prev_observation()
        if p_obs is not None:
            endpoint_diff = np.subtract(self.endpoint_pos, p_obs[-4:-2])
            l_vel = np.dot(self.get_kin_diff(), endpoint_diff) / (np.linalg.norm(endpoint_diff) + 1e-6) / (np.linalg.norm(self.get_kin_diff()) + 1e-6)
#            l_vel = np.sum(np.square(np.subtract(self.get_kin_diff(), endpoint_diff) / self.stepsize))
#            if self.visualize:
#                print('\t\t\t\t\t%5.2f %5.2f -> %5.2f %5.2f' % ( self.get_kin_diff()[0] * 1000, self.get_kin_diff()[1]*1000,  endpoint_diff[0]*1000, endpoint_diff[1]*1000 ))
        
        target = self.get_kin_state()

        l_task = np.sum(np.square(np.subtract(target, self.endpoint_pos)))
        l_energy = np.sum(np.square(np.maximum(np.subtract(action, 0.05), 0)))
        if self.visualize:
            print('%5.2f %5.2f -> %5.2f %5.2f    cost: %4.2f %4.2f %4.2f' % (target[0], target[1], self.endpoint_pos[0], self.endpoint_pos[1], 5*l_task, l_energy/200, l_vel/100))
        
        return 1 - 5 * l_task - l_energy / 200 + l_vel / 200
#        return 1 - 5 * l_task

    def is_done(self):
        obs = self.get_observation()
        p_shoulder, p_elbow = obs[0], obs[2]

        return (
            p_elbow < -0.1 or p_elbow > 0.88 * math.pi or
            p_shoulder < -0.8 * math.pi or p_shoulder > math.pi or
            np.any(np.isnan(self.get_unsanitized_observation()))  # TODO: find out why this happens
        )

    def reset(self):
        self.initiate_phase()
        self.observation_history = {}
        if self.visualize:
            self.plotter = DynamicPlotter([-.5, 1], [-1, 0])
        return super(ArmEnv, self).reset()  # TODO: check if this needs change
    
    def step(self, action):
        try:
            if np.any(np.isnan(action)):
                raise ActionValueError
            self._step(action)
        except (IntegrationError, ActionValueError):
            return self.get_observation(), self.CrashPenalty, True, {}

        self.increment_phase()
        #print(self.get_observation()[-4:])
        self.get_observation()
        if self.plotter and self.istep >= 100:
            self.plotter.add_points(self.endpoint_pos, self.get_kin_state())

        return [ self.get_observation(), self.compute_reward(action), self.is_done(), {} ]
